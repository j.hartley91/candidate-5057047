Feature: Confirm whether a visa is required to visit the UK

  Scenario: Checking Visa requirement of Japan national studying for longer then 6 months
    Given I provide a nationality of Japan
    And I select the reason “Study”
    And I state I am intending to stay for more than 6 months
    Then I will be informed “You’ll need a visa to study in the UK”

  Scenario: Checking Visa requirement of Japan national visiting for tourism
    Given I provide a nationality of Japan
    And I select the reason “Tourism”
    Then I will be informed “You won’t need a visa to come to the UK”

  Scenario: Checking Visa requirement of Russia national visiting for tourism
    Given I provide a nationality of Russia
    And I select the reason “Tourism”
    And I state I am not travelling or visiting a partner or family
    Then I will be informed “You’ll need a visa to come to the UK”