package james.hartley.steps.serenity;

import static com.jayway.restassured.RestAssured.given;

import com.jayway.restassured.response.Response;
import james.hartley.pages.VisaPage;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class EndUserSteps {

    private VisaPage visaPage;
    private final String QUERY_POSTCODE_RESPONSE = "query_postcode_response";

    @Step
    public void enters_nationality(String nationality) {
        visaPage.open();
        visaPage.chooseNationality(nationality);
    }

    @Step
    public void select_reason_for_travel(String reason) {
        visaPage.selectReasonForTravel(reason);
    }

    @Step
    public void i_will_be_informed(String info) {
        visaPage.verifyInfoProvided(info);
    }

    @Step
    public void select_more_then_six_months() {
        visaPage.selectMoreThenSixMonths();
    }

    @Step
    public void select_not_travelling_or_visiting_a_partner_or_family() {
        visaPage.selectNoNotTravellingOrVisitingAPartnerOrFamily();
    }

    @Step
    public void queryPostcode(String postcode) {
        Serenity.setSessionVariable(QUERY_POSTCODE_RESPONSE).to(
                given()
                        .when()
                        .get(String.format("http://api.postcodes.io/postcodes/%s", postcode))
        );
    }

    @Step
    public void verifyResponseCode(int statusCode) {
        Response queryResponse = Serenity.sessionVariableCalled(QUERY_POSTCODE_RESPONSE);
        queryResponse.then().statusCode(statusCode);
    }
}