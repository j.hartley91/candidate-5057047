package james.hartley.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import james.hartley.steps.serenity.EndUserSteps;
import net.thucydides.core.annotations.Steps;

public class VisaSteps {

    @Steps
    private EndUserSteps visa;

    @Given("^I provide a nationality of (.*)$")
    public void givenIProvideANationalityOf(String nationality) {
        visa.enters_nationality(nationality);
    }

    @And("^I select the reason “(.*)”$")
    public void AndISelectTheReason(String reason) {
        visa.select_reason_for_travel(reason);
    }

    @And("^I state I am intending to stay for more than 6 months$")
    public void AndIStateIAmIntendingToStayForMoreThanSixMonths() {
        visa.select_more_then_six_months();
    }

    @And("^I state I am not travelling or visiting a partner or family$")
    public void iStateIAmNotTravellingOrVisitingAPartnerOrFamily() {
        visa.select_not_travelling_or_visiting_a_partner_or_family();
    }

    @Then("^I will be informed “(.*)”$")
    public void iWillBeInformed(String info) {
        visa.i_will_be_informed(info);
    }
}
