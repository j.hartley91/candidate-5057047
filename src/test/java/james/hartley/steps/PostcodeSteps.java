package james.hartley.steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import james.hartley.steps.serenity.EndUserSteps;
import net.thucydides.core.annotations.Steps;

public class PostcodeSteps {

    @Steps
    private EndUserSteps api;

    @When("^I send a get request to api.postcodes.io/postcodes/(\\w+)$")
    public void iSendAGetRequestTo(String postcode) {
        api.queryPostcode(postcode);
    }

    @Then("^I get a (\\d+) response$")
    public void iGetAResponse(int statusCode) {
        api.verifyResponseCode(statusCode);
    }
}
