package james.hartley.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import java.util.List;

import static org.junit.Assert.*;

@DefaultUrl("https://www.gov.uk/check-uk-visa/y")
public class VisaPage extends PageObject {

    @FindBy(id = "response")
    private WebElement Select;

    @FindBy(className = "button")
    private WebElement nextStepButton;

    @FindBy(className = "selection-button-radio")
    private List<WebElement> reasonList;

    @FindBy(className = "result-title")
    private WebElement infoProvided;

    public void chooseNationality(String nationality) {
        Select nationalityDropDown = new Select(Select);
        nationalityDropDown.selectByVisibleText(nationality);
        nextStepButton.submit();
    }

    public void selectReasonForTravel(String reason) {
        for (WebElement element : reasonList) {
            if (element.getText().equals(reason)) {
                element.click();
                nextStepButton.submit();
                return;
            }
        }
        fail(String.format("Reason for travel '%s' not found.", reason));
    }

    public void verifyInfoProvided(String info) {
        assertEquals(info, infoProvided.getText());
    }

    public void selectMoreThenSixMonths() {
        for (WebElement element : reasonList) {
            if (element.getText().equals("longer than 6 months")) {
                element.click();
                nextStepButton.submit();
                return;
            }
        }
        fail("Could not find 'longer than 6 months' option.");
    }

    public void selectNoNotTravellingOrVisitingAPartnerOrFamily() {
        for (WebElement element : reasonList) {
            if (element.getText().equals("No")) {
                element.click();
                nextStepButton.submit();
                return;
            }
        }
        fail("Could not find 'No' option.");
    }
}
